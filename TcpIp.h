#pragma once

#define USE_WIN32 0 // change 0 to 1 when use window system

#if USE_WIN32
	#define _WINSOCK_DEPRECATED_NO_WARNINGS
	#include <iostream>
	#include <winsock2.h>
	#include <string>
	#include <thread>
	#pragma comment (lib,"ws2_32.lib")
#else
	#include <stdio.h>
	#include <stdlib.h>
	#include <iostream>
	#include <string.h>
	#include <time.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <sys/stat.h>
	#include <unistd.h> // close function
	#include <thread>
	#define FALSE false
	#define TRUE true
#endif






class CTCPIP {

private:
	#if USE_WIN32
		SOCKET m_Socket;
		WSADATA m_WsaData;
		SOCKADDR_IN m_Addr;
		SOCKET m_ClientSocket;
		SOCKADDR_IN m_ClientAddr;
	#else
		struct sockaddr_in server_addr, client_addr;
		int server_fd, client_fd;
		socklen_t len;
	#endif
	std::string SOCKET_TYPE;
	std::string OS_TYPE;


public:
	std::string IP;
	unsigned int PORT;
	int nLimitTime;
	bool bConnectionState;
	CTCPIP();
	CTCPIP(std::string ip, unsigned int port, int TimeLimit = 1000);
	CTCPIP(unsigned int port, int TimeLimit = 1000);
	~CTCPIP();

public:
	void Close();
	void Send(char* Buffer, int len);
	void Recv(char* Buffer, int len);

private:
	void CreateSocket();
	

	void Bind();
	void Listen();
	void Accept();
	

public:
	void Connect(int _TimeLimit);
	void ServerOpen();
	void ClientOpen(int _TimeLimit);

};