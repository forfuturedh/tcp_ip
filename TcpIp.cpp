#include "TcpIp.h"

using namespace std;


CTCPIP::CTCPIP()
{

}
CTCPIP::CTCPIP(string ip, unsigned int port, int TimeLimit)
{
	IP = ip;
	PORT = port;
	nLimitTime = TimeLimit;
	bConnectionState = FALSE;
}

CTCPIP::CTCPIP(unsigned int port, int TimeLimit)
{
	PORT = port;
	nLimitTime = TimeLimit;
	bConnectionState = FALSE;
}

CTCPIP::~CTCPIP()
{
	Close();
}


void CTCPIP::CreateSocket()
{	
	#if USE_WIN32
		if (WSAStartup(WINSOCK_VERSION, &m_WsaData) != 0)
		{
			std::cout << "WSAStartup 실패, 에러코드 : " << WSAGetLastError() << std::endl;
			bConnectionState = FALSE;
		}

		// socket(사용할 주소 체계, 소켓 타입, 데이터전송방식)
		//AF_INET : IPv4버전 사용, SOCK_STEAM : 연결지향형, IPPROTO_TCP: TCP방식의 데이터전송방식)
		//m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (m_Socket == INVALID_SOCKET) {
			std::cout << "Socket 생성 실패, 에러코드 : " << WSAGetLastError() << std::endl;
			bConnectionState = FALSE;
		}
	#else
		if((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
			std::cout << "Server : Can't open stream socket" << std::endl;
			bConnectionState = FALSE;
		}
	#endif

}

void CTCPIP::Bind()
{
	#if USE_WIN32
		m_Addr.sin_family = AF_INET;
		m_Addr.sin_port = htons(PORT);
		m_Addr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);

		if (::bind(m_Socket, (SOCKADDR*)&m_Addr, sizeof(m_Addr)) == SOCKET_ERROR)
		{
			std::cout << "bind 실패, 에러코드 : " << WSAGetLastError() << std::endl;
			closesocket(m_Socket);
			WSACleanup();
		}
	#else
		memset(&server_addr, 0x00, sizeof(server_addr));
		server_addr.sin_family = AF_INET;
		server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		server_addr.sin_port = htons(PORT);

		if ( bind(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0){
			std::cout << "bind faild" << std::endl;
			close(server_fd);
		}
	#endif
}


void CTCPIP::Listen()
{
	#if USE_WIN32
		if (listen(m_Socket, SOMAXCONN) != 0) // (소켓핸들, 접속 대기자수) / SOMAXCONN --> 무한
		{
			std::cout << "Listen 모드 설정 실패, 에러코드 : " << WSAGetLastError() << std::endl;
			closesocket(m_Socket);
			WSACleanup();
		}
		else
		{
			std::cout << "서버 시작합니다. " << std::endl;
			std::cout << "클라이언트 접속을 기다리고 있습니다.... " << std::endl;
		}
	#else
		if (listen(server_fd,5) < 0){
			close(server_fd);
		}
		else{
			std::cout <<" wait client" << std::endl;
		}
	#endif
}

void CTCPIP::Accept()
{
	#if USE_WIN32
		m_ClientAddr;
		int m_ClientAddrSize = sizeof(m_ClientAddr);
		m_ClientSocket = accept(m_Socket, (SOCKADDR*)&m_ClientAddr, &m_ClientAddrSize);
		if (m_ClientSocket == INVALID_SOCKET)
		{
			cout << "접속 승인 실패, 에러코드 : " << WSAGetLastError() << endl;
			closesocket(m_Socket);
			WSACleanup();
		}
		else
		{
			bConnectionState = TRUE;
			cout << "클라이언트와 연결되었습니다." << endl;
			cout << " 클라이언트 접속 IP : " << inet_ntoa(m_ClientAddr.sin_addr) << " Port: " << ntohs(m_ClientAddr.sin_port) << endl;

		}
	#else
		len = sizeof(client_addr);
		client_fd = accept(server_fd,(struct sockaddr *)&client_addr, &len);
		if(client_fd < 0){
			std::cout << "accept failed" << std::endl;
			close(client_fd);
		}
		else{
			char temp[20];
			inet_ntop(AF_INET, &client_addr.sin_addr.s_addr, temp, sizeof(temp));
			bConnectionState = TRUE;
			std::cout << "client IP : " << temp << std::endl;
		}
	#endif
}

void CTCPIP::Close()
{
	#if USE_WIN32
		if (closesocket(m_Socket) != 0)
		{
			cout << "소켓 제거 실패" << endl;
		}
		if (WSACleanup() != 0)
		{
			cout << "WSACleanup 실패 " << endl;
		}
	#else
		close(server_fd);
	#endif


}

void CTCPIP::ServerOpen()
{
	SOCKET_TYPE = "SERVER";
	CreateSocket();
	Bind();
	Listen();
	Accept(); // 클라이언트가 접속할때까지 행 걸림
}


void CTCPIP::Connect(int _TimeLimit)
{
	#if USE_WIN32
		u_long block = 1; // 소켓은 생성할때 blocking mode가 default , block변수값이 0 -> blockking mode , 0이 아닌 다른 수는 -> non-blockking mode
		if (ioctlsocket(m_Socket, FIONBIO, &block) == SOCKET_ERROR) // socket 입출력 모드 변경
		{
			closesocket(m_Socket);
			cout << " non-blocking FAIL !!" << endl;
			bConnectionState = FALSE;
			return;
		}

		int ret = connect(m_Socket, (SOCKADDR*)&m_Addr, sizeof(m_Addr)); // socket이 non-block상태로 connect를 요청하면 ERROR반환 ( -1)
		if (ret == SOCKET_ERROR)
		{
			int errorMsg = WSAGetLastError(); // error code 10035 면 소켓이 non-block상태로 동작 중이다 라는 에러 메세지임
			if (errorMsg != WSAEWOULDBLOCK)
			{
				cout << "connect Error" << endl;
				// connection failed
				closesocket(m_Socket);
				bConnectionState = FALSE;
				return;
			}

			// connection pending
			fd_set setW, setE;

			FD_ZERO(&setW);
			FD_SET(m_Socket, &setW);


			setE = setW;

			timeval time_out = { 0 };
			time_out.tv_sec = long(_TimeLimit / 1000.);
			time_out.tv_usec = (_TimeLimit % 1000) * 1000;

			// 랜선이 뽑혔을 경우 E에 기록된다고함, 그러나 현재 setE자체를 소켓 생성할때 초기화하기땜에 안됨(한번 생성된 소켓을 재활용 할 수는 없나?)
			// 소켓에 write됬을때 setW cnt가 1
			// 
			int ret = select(0, NULL, &setW, &setE, &time_out); // return 0 : TIME out, -1 : 오류, 0보다 큰 수 변한 FD 갯수
			if (ret <= 0)
			{
				// select() failed or connection timed out
				closesocket(m_Socket);
				if (ret == 0) {
					WSASetLastError(WSAETIMEDOUT);
					cout << "NOT Connected !" << endl;
				}
				return;
			}

			if (FD_ISSET(m_Socket, &setE))
			{
				// connection failed
				cout << " Check the LAN Port or Server!!  " << endl;
				closesocket(m_Socket);
				bConnectionState = FALSE;
				return;
			}
			else if (FD_ISSET(m_Socket, &setW))
			{
				cout << " Server Connected !! " << endl;
				bConnectionState = TRUE;
			}


		}

		// put socked in blocking mode...
		if (bConnectionState == TRUE)
		{
			block = 0;
			if (ioctlsocket(m_Socket, FIONBIO, &block) == SOCKET_ERROR)
			{
				cout << " Socket blocking Fail !" << endl;
				closesocket(m_Socket);
				bConnectionState = FALSE;
				return;
			}

		}
		return;
	#else

	#endif

}



void CTCPIP::ClientOpen(int _TimeLimit)
{
	#if USE_WIN32
		SOCKET_TYPE = "CLIENT";
		while (bConnectionState == FALSE)
		{
			CreateSocket();

			memset((char *)&m_Addr, 0, sizeof(m_Addr));
			m_Addr.sin_family = AF_INET;
			m_Addr.sin_addr.S_un.S_addr = inet_addr(IP.c_str());
			m_Addr.sin_port = htons(PORT);

			Connect(_TimeLimit);
		}
	#else
	#endif

}

void CTCPIP::Recv(char *Buffer, int len)
{
	#if USE_WIN32
		int ret = 0;

		if (SOCKET_TYPE == "SERVER") {
			ret = recv(m_ClientSocket, (char *)Buffer, len, 0);
		}
		else if (SOCKET_TYPE == "CLIENT") {
			ret = recv(m_Socket, (char *)Buffer, len, 0);
		}
			
		if (ret == 0 || ret == SOCKET_ERROR)
		{
			bConnectionState = FALSE;
		}
		else
		{

			cout << "Recived Data : " << Buffer << endl;

		}
	#else
		int ret = 0;
		if(SOCKET_TYPE == "SERVER"){
			ret = recv(client_fd,(char *)Buffer, len, 0);
		}
		else if(SOCKET_TYPE == "CLIENT"){
			ret = recv(server_fd,(char *)Buffer, len, 0);
		}
		if (ret == 0 || ret == -1)
		{
			bConnectionState = FALSE;
		}
		else
		{
			cout << "Recived Data : " << Buffer << endl;
		}
	#endif



}


void CTCPIP::Send(char* Buffer, int len)
{
	#if USE_WIN32
		int ret = 0;
		if (SOCKET_TYPE == "SERVER") {
			ret = send(m_ClientSocket, (char *)Buffer, len, 0);
		}
		else if (SOCKET_TYPE == "CLIENT") {
			ret = send(m_Socket, (char *)Buffer, len, 0);
		}
		
		if (ret == 0 || ret == SOCKET_ERROR)
		{
			bConnectionState = FALSE;
			ClientOpen(1000);
		}
		else
		{
			cout << "Send Data : " << Buffer << endl;
		}
	#else
		int ret;
		if (SOCKET_TYPE == "SERVER") {
			ret = send(client_fd, (char *)Buffer, len, 0);
		}
		else if (SOCKET_TYPE == "CLIENT") {
			ret = send(server_fd, (char *)Buffer, len, 0);
		}
		
		if (ret == 0 || ret == -1)
		{
			bConnectionState = FALSE;
		}
		else
		{
			cout << "Send Data : " << Buffer << endl;
		}
	#endif

}
